import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
@Injectable({
  providedIn: 'root',
})
export class LocationService{
  constructor(private httpClient: HttpClient){}
  getLocationPCs(location_aux) {
    return this.httpClient.post(environment.GET_PCS_URL, location_aux);
  }
  savePCsStatus(pcs){
  	return this.httpClient.put(environment.PCS_URL, pcs);
  }
}