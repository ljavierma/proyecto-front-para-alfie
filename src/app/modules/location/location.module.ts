import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LocationRoutingModule } from './location-routing.module';
import { LocationComponent } from './location.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    LocationRoutingModule
  ],
  declarations: [
    LocationComponent
  ]
})
export class LocationModule {}