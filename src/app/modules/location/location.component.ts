import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from './location.service'
import { PC } from './pc.model'
@Component({
  selector: 'location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  /**
   * Array that is going to be used to storage the PCs that are showed in the catalogue.
   */
  public pcs: PC[];
  /**
   * Var that is going to be used to display the name of the location.
   */
  public location_name: string;
  /**
   * Variable that determinates if the page has a petition in progress.
   */
  public loading: boolean = false;
  /**
   * Constructor of the location component.
   * @param { ActivatedRoute } activatedRoute Element that is used to get the route and query params.
   * @param { LocationService } locationService Service that allow us to make http petitions to get the pcs list and update its information.
   * @param { Router } router Element that is used to direct the page to other views.
   */
  constructor(private activatedRoute: ActivatedRoute, private locationService: LocationService, private router: Router){}
  /**
   * Function provided for the OnInit interface, it defines the actions to execute when the component is loaded.
   * It is specified that it should get the PCs list and set the location name.
   */
  ngOnInit(): void{
    this.getPCs();
    this.setLocationName();
  }
  /**
   * Function that looks for the location id value in the route then gets the PCs of that location.
   */
  getPCs(): void{
    this.loading  = true;
    this.activatedRoute.params.subscribe(
      params => {
        let location_aux = {
          location: +params['id']
        }
        this.locationService.getLocationPCs(location_aux).subscribe(
          (pcs: PC[]) => {
            this.pcs = pcs;
            this.loading  = false;
          },
          error => {
            this.loading  = false;
            alert('Ha habido un error al intentar obtener la información de la sucursal');
          }
        );
      }
    );
  }
  /**
   * Function that looks for the location name in the query param, then sets it in the location_name variable.
   */
  setLocationName(): void{
    this.activatedRoute.queryParams.subscribe(
      query_params => {
        this.location_name = query_params['name'];
      }
    );
  }
  /**
   * Function that looks for a pc in the pcs array.
   * @param { number } pc_id The id of the pc that is being looked for.
   * @return Element with the id that matches the one that is being received.
   */
  getPCById(pc_id: number): PC{
    for (var i = 0; i < this.pcs.length; i++) {
      if (this.pcs[i].id === pc_id) {
        return this.pcs[i];
      }
    }
  }
  /**
   * Function that switch the isWork value to the oposite of it.
   * This function is called when a select box value is changed.
   * @param { number } pc_id The id of the pc that is being looked for.
   */
  switchIsWorkStatus(pc_id: number): void{
    let pc_aux = this.getPCById(pc_id);
    pc_aux.isWork = !pc_aux.isWork;
  }
  /**
   * Function that saves in the server the status of the PCs.
   * If the information is saved with success the page is directed to the home view.
   */
  savePCsStatus(): void{
    this.loading  = true;
    let pcs_status = [];
    for (var i = 0; i < this.pcs.length; i++) {
      let pc_aux = {
        id: this.pcs[i].id,
        isWork: this.pcs[i].isWork
      }
      pcs_status.push(pc_aux);
    }
    this.locationService.savePCsStatus(pcs_status).subscribe(
      response => {
        this.loading  = false;
        this.router.navigate(['/']);
        alert('Información guardada con éxito')
      },
      error => {
        this.loading  = false;
        alert('Ha habido un error al intentar guardar la información de las PCs')
      }
    );
  }
}