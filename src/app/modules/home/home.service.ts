import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
@Injectable({
  providedIn: 'root',
})
export class HomeService{
  constructor(private httpClient: HttpClient){}
  getLocations() {
  	return this.httpClient.get(environment.GET_LOCATIONS_URL);
  }
}