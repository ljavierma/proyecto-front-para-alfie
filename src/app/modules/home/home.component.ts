import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from './home.service';
import { Location } from './location.model';
@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  /**
   * Array that is going to be used to storage the locations that are showed in the select.
   */
  public locations: Location[];
  /**
   * Variable that determinates if the page has a petition in progress.
   */
  public loading: boolean = false;
  /**
   * Constructor of the home component.
   * @param { HomeService } homeService Service that allow us to make http petitions to get the locations.
   * @param { Router } router Element that is used to direct the page to other views.
   */
  constructor(private homeService: HomeService, private router: Router){}
  /**
   * Function provided for the OnInit interface, it defines the actions to execute when the component is loaded.
   * It is specified that it should look for the locations on the server.
   */
  ngOnInit(): void{
    this.getLocations();
  }
  /**
   * Function that ask to the server for the locations and in case that the petition goes well the locations are storaged in the "locations" array.
   */
  getLocations(): void{
    this.loading = true;
    this.homeService.getLocations().subscribe(
      (locations: Location[]) => {
        this.locations = locations;
        this.loading = false;
      },
      error => {
        this.loading = false;
        alert('Ha ocurrido un error al obtener las sucursales');
      }
    );
  }
  /**
   * Funcion that is called when an option of the select is selected, it directs the page to the location details view sending the location id as a router parameter and the location name as a query.
   * @param { any } location The item that the select manage.
   */
  goToLocationDetails(location: any): void{
    let location_name_aux = '';
    for (var i = 0; i < this.locations.length; i++) {
      if (this.locations[i].id === location.value) {
        location_name_aux = this.locations[i].name;
      }
    }
    this.router.navigate([('location/' + location.value)], {queryParams: { name: location_name_aux }});
  }
}