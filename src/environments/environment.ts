const BASE_URL = 'http://api.alfie.pro/';
export const environment = {
  production: false,
  GET_LOCATIONS_URL: BASE_URL + 'getlocations',
  GET_PCS_URL: BASE_URL + 'getpcs',
  PCS_URL: BASE_URL + 'pcs'
};